import 'package:flutter/material.dart';

class PageOne extends StatefulWidget {
  var data;
  PageOne(this.data);
  _PageOneState createState() => _PageOneState(this.data);
}

class _PageOneState extends State<PageOne> {
  var receivedData;
  _PageOneState(this.receivedData);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 1'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Name: ${receivedData['name']}"),
            Text("Email: ${receivedData['email']}"),
            ElevatedButton(
              child: Text('Go back to dashboard'),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }
}
