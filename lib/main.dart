import 'package:flutter/material.dart';
import 'page1.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

void main() {
  runApp(MaterialApp(
    home: Dashboard(),
  ));
}

class Dashboard extends StatefulWidget {
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List data;
  List unfilterData;

  Future<bool> _getPosts() async {
    var jsonText = await rootBundle.loadString('assets/data.json');
    setState(() {
      data = json.decode(jsonText);
      print(data[0]);
    });
    this.unfilterData = data;
    return true;
  }

  @override
  void initState() {
    super.initState();
    this._getPosts();
  }

  searchData(input) {
    var strExists = input.length > 0 ? true : false;
    if (strExists) {
      var filterData = [];
      for (var i = 0; i < this.unfilterData.length; i++) {
        var name =
            unfilterData[i]['name'].toUpperCase(); // ignore case sensitive
        if (name.contains(input.toUpperCase())) {
          filterData.add(unfilterData[i]);
        }
      }
      setState(() {
        this.data = filterData;
      });
    } else {
      setState(() {
        this.data = this.unfilterData;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('List View Search'),
          centerTitle: true,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextField(
              decoration: InputDecoration(hintText: "Search Name"),
              onChanged: (String str) {
                print(str);
                this.searchData(str);
              },
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(8.0),
                itemCount: this.data.length == null ? 0 : this.data.length,
                itemBuilder: (BuildContext context, int index) {
                  var name = data[index]['name'];
                  var email = data[index]['email'];
                  return ListTile(
                    leading: CircleAvatar(
                      child: Text(name[0].toUpperCase()),
                    ),
                    title: Text(name),
                    subtitle: Text(email),
                    onTap: () {
                      Route route = MaterialPageRoute(
                          builder: (context) => PageOne(data[index]));
                      Navigator.push(context, route);
                    },
                  );
                },
              ),
            )
          ],
        ));
  }
}
